// Config - Helper struct to pass to a task object
const Config = (
  Duration = null,
  Title = '',
  Input = null,
  Output = null,
  ID = '',
  Interface = null,
  Setup = null,
  Dependency = null
) => {
  return {
    Class: 'configMeta',
    Duration,
    Title,
    Input,
    Output,
    ID,
    Interface,
    Setup,
    Dependency
  }
}

export const TESTING_ID = 'testing'

export default Config
