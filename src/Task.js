export class Task {
  constructor (name, config) {
    this.name = name
    if (config.Class !== 'configMeta') {
      // panic
    }
    for (const k of Object.keys(config)) {
      this[k] = config[k]
    }

    this.Status = false
    if (this.Dependency != null) {
      this.Locked = true
    }
    this.Update = this.Update.bind(this)
  }

  // Getter Functions
  Duration () {
    return this.Duration
  }

  ID () {
    return this.ID
  }

  Input () {
    return this.Input
  }

  Output () {
    return this.Output
  }

  Interface () {
    return this.Interface
  }

  Setup () {
    return this.Setup
  }

  Dependency () {
    return this.Dependency
  }

  Update () {

  }

  // Dependency should be a call back that is linked to another task
  // Once That task is marked completed, it should trigger its own notify in order
  // to then trigger this Dependency to mark itself as true
  addDependency (Dependency) {
    this.locked = true
    this.Dependency = Dependency
    this.Dependency = this.Dependency.bind(this)
  }
}

export default Task
