const Duration = (delta, unit) => {
  const obj = {
    delta,
    unit: ''
  }
  const ts = (o) => { return `${o.delta} ${o.unit}` }
  switch (unit.toLowerCase()) {
    case 'm':
      obj.unit = 'minutes'
      break
    case 'h':
      obj.unit = 'hours'
      break
    default:
      return new Error('Unknown time unit')
  }

  // TODO: This be terrible, fix it somehow?
  obj.toString = () => { return ts(obj) }

  return obj
}
export default Duration
