class Project {
  constructor (name) {
    this.Tasks = []
    this.CompletedTasks = []
    this.PendingTasks = []

    this.Visualize = this.Visualize.bind(this)
    this.AddTask = this.AddTask.bind(this)
    // this.CreatedAt=time.Now()
    // this.createdBy=getMe()
    this.progress = 0
  }

  async _updateProgress () {
    await this.Tasks.forEach(t => { t.update() })
    this.CompletedTasks = this.Tasks.filter(t => { return t.Status === true })
    this.PendingTasks = this.Tasks.filter(t => { return t.Status === false })
    this.progress = this.CompletedTasks.length / this.PendingTasks.length
  }

  Visualize () {}

  AddTask (task) {
    this.Tasks.append(task)
  }

  _json () {
    const obj = {
      Tasks: this.Tasks.map(t => { return t.ID() }),
      CompletedTasks: this.CompletedTasks,
      PendingTasks: this.PendingTasks
    }

    return JSON.parse(obj)
  }
}

export default Project
