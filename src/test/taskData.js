import Config ,{ TESTING_ID } from '../Config.js'
const data = [
  {
    Name: 'Passing 1',
    Data:{
       Config: new Config(
      // TODO: Duration needs to be an object type
      15,
      'passing_1',
      // TODO: Make a test object for input
      null,
      null,
      TESTING_ID,
      null,
      null,
      null
    )
  },
    Expected: ''
  }
  // Check null on:
  // Input
  // Output
  // ID
  // Interface
]

export default data
