import Duration from '../Duration.js'
test("Duration toString works correctly",()=>{
  const t = Duration(5,'m')
  expect(t.toString()).toBe('5 minutes')
})
